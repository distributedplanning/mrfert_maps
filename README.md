# README #

This package contains a list of maps used to test the mrfert planner.
For each test, all the launch files are in a separate folder, while the launch folder contains the aggregated launch file.

To test, use:

```
roslaunch mrfert_maps cross4_gazebo.launch
```

and once gazebo is started,

```
roslaunch mrfert_maps cross4_planner.launch
```

Other tests have the same structure, such as 

```
roslaunch mrfert_maps door3_gazebo.launch
roslaunch mrfert_maps door3_planner.launch
```